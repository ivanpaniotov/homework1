package edu.dnu.fpm.pz.calc.interfaces;

/**
 * This is calc interfaces
 *
 */
public interface Calc
{
    double addition(double a, double b);
    double substraction(double a, double b);
    double multiplication(double a, double b);
    double division(double a, double b);
}
